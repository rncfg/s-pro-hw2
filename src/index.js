
const fs = require('fs')

function writeToFile(argv) {
  const newFileName = argv[2]
  const data = argv.slice(3).join(' ')

  fs.writeFile(newFileName, data, { flag: 'a+' }, (err) => {
    if (err) throw new Error('Error, try again.');
    console.log("Operation successfull");
  })
}

writeToFile(process.argv)

